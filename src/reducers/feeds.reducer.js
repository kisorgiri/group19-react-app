import { ADD_FEED, SET_IS_LOADING, SET_PAGE_NUMBER, FETCH_FEED } from './../actions/feeds/type';
const initialState = {
    feeds: [],
    feed: {},
    isLoading: false,
    pageNumber: 1,
    pageCount: 5
}

// action will contain type and payload
// reducer is pure function that will return new state everytime a dispactch is called
const feedsReducer = (state = initialState, action) => {
    console.log('action >>>', action);
    switch (action.type) {
        case FETCH_FEED:
            return {
                ...state,
                feeds: action.payload
            }

        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }
        default:
            return state;
    }
}


export default feedsReducer