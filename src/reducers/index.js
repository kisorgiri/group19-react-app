import { combineReducers } from 'redux';
import FeedsReducer from './feeds.reducer';



export default combineReducers({
    feeds: FeedsReducer
})