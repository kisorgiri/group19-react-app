import moment from 'moment';


export function formatDate(date) {
    return moment(date).format('MM DD YYYY');
}
export function formatTime(date) {
    return moment(date).format('hh:mm a');
}
export function relativeTime(date) {
    return moment(date).startOf('hour').fromNow();       // 38 minutes ago

}