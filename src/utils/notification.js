import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg);
}
function showInfo(msg) {
    toast.info(msg);
}
function showWarning(msg) {
    toast.warn(msg);
}

function showError(msg) {
    toast.error(msg);
}


function handleError(err) {
    debugger;
    let error = err.response || err;
    let msg = "something went wrong"
    if (error && error.data && error.data.msg) {
        msg = error.data.msg
    }
    else if (err.msg) {
        msg = error.msg;
    }

    // check what comes in error
    // parse the incoming error message
    // show them in UI 
    showError(msg)
}

export default {
    handleError,
    showSuccess,
    showInfo,
    showWarning
}