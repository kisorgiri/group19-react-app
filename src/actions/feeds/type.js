export const FETCH_FEED = 'FETCH_FEED';
export const ADD_FEED = 'ADD_FEED';
export const SET_IS_LOADING = 'SET_IS_LOADING';
export const SET_PAGE_NUMBER = 'SET_PAGE_NUMBER';