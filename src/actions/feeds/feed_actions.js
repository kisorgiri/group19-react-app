import httpClient from "../../utils/http-client";
import { FETCH_FEED, SET_IS_LOADING, SET_PAGE_NUMBER } from "./type";
import notification from "../../utils/notification";

const fetchFeeds = (pageNumber = 1, perPage = 5) => {
    return dispatch => {
        console.log('calling BE', pageNumber);
        dispatch(loadingAction(true))
        // http call
        httpClient
            .post('feed/search', {}, false, {
                pageNumber: pageNumber,
                pageSize: perPage
            })
            .then((data) => {
                console.log('http call result', data);
                console.log('dispatched should be called');
                dispatch({
                    type: FETCH_FEED,
                    payload: data.data
                })
            })
            .catch(err => notification.handleError(err))
            .finally(() => {
                dispatch(loadingAction(false))

            });
    }
}

export const changePage = (pageNumber) => (dispatch) => {
    console.log('here at changePage', pageNumber);
    dispatch({
        type: SET_PAGE_NUMBER,
        payload: pageNumber
    })
}
export const loadingAction = (isloading) => ({
    type: SET_IS_LOADING,
    payload: isloading
})

export default fetchFeeds;

