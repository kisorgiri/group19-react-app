import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';

const mainDiv = document.getElementById('root');

ReactDOM.render(<App />, mainDiv);



// es6 features
// import and export

// export ==> two way of exporting in es6
// named export  eg. export const a, 
// export class test

// default export export default ram;

// import in es6
// import { Test, View, } from './app.js'
// import Ram from './app.js';
// console.log('Ram >>', Ram);

// object destructure
// const obj = {
//     name: 'broadway',
//     addr: 'tinkune'
// }
// let {name} = obj;

// arrow notation function
// const welcome () => {
//     const a = 'i am inside welcome';
//     const hi = () => {
//         console.log('a ')

//     }
// }

// spread operator rest paramter
// ... spread operator

// mutation and immutation


// glossary
// props ==> input data of component
// eg <welcome a="hi" age="33" isLoogedIn="3333">

// state==> mutable data 
// state is data of component
// component
// functional component
// class based component
// stateful component
// stateless component

// controlled Input
// uncontrolled Input

// fragments















