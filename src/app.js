import React, { Component } from 'react';
// import Login from './components/login/login';
// import { Register } from './components/register/register.component';
import RoutingConfig from './app-routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import store from './store';
export class App extends Component {
    // class based component are either stateful or stateless
    // class based component must have render method
    render() {
        // try to keep all component logic inside render
        return (
            <div className="container">
                <Provider store={store}>
                    <RoutingConfig />
                </Provider>
                <ToastContainer />
            </div>
        )
    }
}