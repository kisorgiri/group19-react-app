import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';
import Login from './components/login/login';
import { Register } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import Navbar from './components/Header/navbar.component';
import { Sidebar } from './components/sidebar/sidebar.component';
import { AddFeedComponent } from './components/feeds/addFeed.component';
import { ViewFeedComponent } from './components/feeds/viewFeedCompoent';
import { EditFeedComponent } from './components/feeds/editFeedComponent';
import { SearchFeedComponent } from './components/feeds/searchFeedComponent';
import PageNotFound from './components/views/pagenotfound.component';
import { LiveFeedComponent } from './components/feeds/live-feeds/live-feed.component';
import { ForgotPassword } from './components/forgot-password/forgot-password.component';
import { ResetPassword } from './components/reset-password/reset-password.component';
import { ChatComponent } from './components/chat/chat.component';
// stateless component

const ProtectedRoute = ({ component: Component, ...rest }) => {

    return (
        <Route path={rest.path} render={(props) => (
            localStorage.getItem('token')
                ? <>
                    <div className="navbar">
                        <Navbar isLoggedInUser="true" />
                    </div>
                    <div className="sidebar">
                        <Sidebar />
                    </div>
                    <div className="content">
                        <Component {...props} />
                    </div>
                </>
                : <Redirect to="/login" /> // TO DO additional attributes 
        )} >

        </Route>
    )
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route path={rest.path} render={(props) => (
            <>
                <div className="navbar">
                    <Navbar isLoggedInUser={false} />
                </div>
                {/* <div className="sidebar">
                    <Sidebar />
                </div> */}
                <div className="content">
                    <Component {...props} />
                </div>
            </>
        )} >

        </Route>
    )
}

const Routing = (props) => {

    return (
        <Router>
            {/* registration of route  */}
            <Switch>
                <PublicRoute exact path='/' component={LiveFeedComponent}></PublicRoute>
                <PublicRoute exact path='/login' component={Login}></PublicRoute>
                <PublicRoute path='/register' component={Register}></PublicRoute>
                <PublicRoute path='/forgot-password' component={ForgotPassword}></PublicRoute>
                <PublicRoute path='/reset-password/:token' component={ResetPassword}></PublicRoute>
                <ProtectedRoute path="/dashboard" component={DashboardComponent} />
                <ProtectedRoute path="/add-blog" component={AddFeedComponent} />
                <ProtectedRoute path="/view-blog" component={ViewFeedComponent} />
                <ProtectedRoute path="/edit-blog/:id" component={EditFeedComponent} />
                <ProtectedRoute path="/notification" component={DashboardComponent} />
                <ProtectedRoute path="/search-blog" component={SearchFeedComponent}></ProtectedRoute>
                <ProtectedRoute path="/chat" component={ChatComponent}></ProtectedRoute>
                <PublicRoute component={PageNotFound}></PublicRoute>
            </Switch>
        </Router >
    )
}

export default Routing;