import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import http from './../../utils/http-client';
import notification from './../../utils/notification';
import { Loader } from '../../utils/loader';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                username: null,
                password: null,
            },
            error: {
                username: '',
                password: ''
            },
            validForm: false,
            isSubmitting: false
        }
    }

    /**
     * handle changes in form input
     * @param {object} e 
     */
    handleChange(e) {
        const { name, value } = e.target;
        this.setState((previousState) => ({
            data: {
                ...previousState.data,
                [name]: value
            }
        }), () => this.validateErrors(name));
    }

    validateErrors(fieldName) {
        let err;
        switch (fieldName) {
            case 'username':
                err = this.state.data[fieldName]
                    ? ''
                    : 'username is required';
                break;
            case 'password':
                err = this.state.data[fieldName]
                    ? ''
                    : 'password is required';
                break;
            default:
                break;
        }

        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: err
            }
        }), () => {
            this.validateForm();
        })
    }

    validateForm() {
        const errors = Object
            .values(this.state.error)
            .filter(err => err);

        let isValidForm = errors.length
            ? false
            : true;

        this.setState({
            validForm: isValidForm
        });
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true,
        })
        http.post('/auth/login', this.state.data)
            .then(data => {
                notification.showSuccess("Welcome " + data.data.user.username);
                localStorage.setItem('user', JSON.stringify(data.data.user));
                localStorage.setItem('token', data.data.token);
                this.props.history.push(`/dashboard`);
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false,
                })
                notification.handleError(err);
            })

    }

    render() {

        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">logging in...</button>
            : <button disabled={!this.state.validForm} className="btn btn-primary" type="submit">Login</button>
        return (
            <div>            
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" id="username" name="username" onChange={this.handleChange.bind(this)} placeholder="username" />
                    <p>{this.state.error.username}</p>
                    <label htmlFor="password" >Password</label>
                    <input className="form-control" type="password" placeholder="Password" onChange={this.handleChange.bind(this)} name="password" />
                    <p>{this.state.error.password}</p>
                    <br />
                    {btn}
                </form>
                <p>Don't have an account</p>
                <p>Register <Link to="/register">here</Link></p>
                <p><Link to="/forgot-password">forgot password?</Link></p>
            </div>
        )
    }
}

export default Login;


// life cycle hook
// 