import React from 'react';

const PageNotFound = (props) => {
    return (
        <>
            <h2>Page Not Found</h2>
            <img src='/images/page.png' alt="page not found" width="600px" ></img>

        </>
    )
}

export default PageNotFound;