import React, { Component } from 'react';
import http from './../../utils/http-client';
import notify from './../../utils/notification';


export class AddFeedComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: {},
            error: {},
            validForm: false,
            isSubmitting: false
        }
    }

    handleChange = (e) => {
        let { type, name, value } = e.target;
        if (type === 'file') {
            value = e.target.files
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateErrors(name)
        });
    }
    validateErrors(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'title':
                errMsg = this.state.data[fieldName].length
                    ? ''
                    : 'title is required';
                break;
            case 'description':
                errMsg = this.state.data[fieldName].length
                    ? this.state.data[fieldName].length > 15
                        ? ''
                        : 'Minimun characters must be 15'
                    : 'Description is required';
                break;

            default:
                break;
        }

        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            this.validateForm();
        })
    }

    validateForm() {
        let errors = Object
            .values(this.state.error)
            .filter(err => err);
        let isValidForm = errors.length ? false : true;
        this.setState({
            validForm: isValidForm
        });
    }

    handleSubmit = (e) => {
        let url = `${process.env.REACT_APP_BASE_URL}/feed?token=${localStorage.getItem('token')}`
        console.log('url >>', url);
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        http.upload({
            url: url,
            method: 'POST',
            data: this.state.data,
            files: this.state.data.image
        })
            .then(data => {
                this.props.history.push('/view-blog');
                notify.showSuccess('New Blog added');
            })
            .catch(err => {
                console.log('here at error,,,', err);
                notify.handleError(JSON.parse(err));
                this.setState({
                    isSubmitting: false
                })
            })
        // http.post('/feed', this.state.data, true)
        //     .then(data => {
        //         this.props.history.push('/view-blog');
        //         notify.showSuccess('New Blog added');
        //     })
        //     .catch(err => {
        //         notify.handleError(err);
        //         this.setState({
        //             isSubmitting: false
        //         })
        //     })
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">Submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">Submit</button>
        return (
            <>
                <h2>Add New Blog</h2>
                <p>Express your ideas here</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="title">Title</label>
                    <input className="form-control" id="title" name="title" placeholder="Title" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.title}</p>
                    <label htmlFor="description">Description</label>
                    <input className="form-control" id="description" name="description" placeholder="Description" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.description}</p>
                    <label>Choose Image</label>
                    <input type="file" className="form-control" onChange={this.handleChange} name="image"></input>
                    <br />
                    {btn}
                </form>
            </>
        )
    }
}