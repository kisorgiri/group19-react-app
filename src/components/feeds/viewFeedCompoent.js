import React, { Component } from 'react';
import http from './../../utils/http-client';
import notify from './../../utils/notification';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { Loader } from '../../utils/loader';

export class ViewFeedComponent extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: false,
            feeds: [],

        };
    }

    formatDate(date) {
        return moment(date).format('MM DD YYYY');
    }
    formatTime(date) {
        return moment(date).format('hh:mm a');
    }

    handleRemove(id, i) {
        // TODO take confirmation from user
        // confirm('Are you sure to remove');
        http.delete(`feed/${id}`, true)
            .then(data => {
                this.state.feeds.splice(i, 1);
                this.setState({
                    feeds: this.state.feeds
                })
                notify.showInfo("Feed Removed");
            })
            .catch(err => {
                notify.handleError(err);
            })
    }

    componentDidMount() {
        if (this.props.incomingData) {
            this.setState({
                feeds: this.props.incomingData
            });
        } else {
            this.setState({
                isLoading: true
            });
            http.get('/feed', true)
                .then((data) => {
                    this.setState({
                        feeds: data.data
                    });
                })
                .catch(err => {
                    notify.handleError(err);
                })
                .finally(() => {
                    this.setState({
                        isLoading: false
                    });
                })
        }


    }

    render() {
        const IMG_URL = process.env.REACT_APP_IMG_URL;
        console.log('img URL >>', IMG_URL);
        let rowData = this.state.feeds.map((item, i) => {
            let actions = this.props.incomingData
                ? ''
                : <td>
                    <Link to={`/edit-blog/${item._id}`}>
                        <i className="material-icons">edit</i>
                    </Link>
                    <i className="material-icons" onClick={() => this.handleRemove(item._id, i)}>
                        <button className="btn btn-danger">delete
                        </button>
                    </i>
                </td>

            return (
                <tr key={item._id}>
                    <td>{i + 1}</td>
                    <td>{item.title}</td>
                    <td> <img src={`${IMG_URL}/${item.image}`} alt="feed.img" width="200px"></img> </td>
                    <td>{this.formatDate(item.createdAt)}</td>
                    <td>{this.formatTime(item.createdAt)}</td>
                    {actions}
                </tr >
            )
        })
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {rowData}
                </tbody>
            </table>
        return (
            content
        )
    }
}