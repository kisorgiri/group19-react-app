import React, { Component } from 'react';
import httpClient from '../../utils/http-client';
import notification from '../../utils/notification';
import { Loader } from '../../utils/loader';


export class EditFeedComponent extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            isSubmitting: false,
            data: {

            },
            error: {

            },
            validForm: true
        }
    }

    handleChange = (e) => {
        let { type, name, value } = e.target;
        if (type === 'file') {
            value = e.target.files
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }

    handleSubmit = (e) => {
        let url = `${process.env.REACT_APP_BASE_URL}/feed/${this.state.data._id}?token=${localStorage.getItem('token')}`

        e.preventDefault();
        this.setState({
            isSubmitting: true,
        })
        httpClient.upload({
            url: url,
            method: "PUT",
            files: this.state.data['new-image'],
            data: {
                ...this.state.data,
                user: this.state.data.user._id
            },
        })
            .then((data) => {
                this.props.history.push('/view-blog');
                notification.showInfo("Update successfull");
            })
            .catch(err => {
                notification.handleError(err);
                this.setState({
                    isSubmitting: false,
                })
            })
        // httpClient.put(`/feed/${this.state.data._id}`, this.state.data, true)
        //     .then((data) => {
        //         this.props.history.push('/view-blog');
        //         notification.showInfo("Update successfull");
        //     })
        //     .catch(err => {
        //         notification.handleError(err);
        //         this.setState({
        //             isSubmitting: false,
        //         })
        //     })
    }

    componentDidMount() {
        this.id = this.props.match.params['id'];

        httpClient.get(`feed/${this.id}`, true)
            .then((response) => {
                this.setState({ data: response.data })
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false,
                })
            })
    }

    render() {
        let imgUrl = process.env.REACT_APP_IMG_URL;
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">Submitting...</button>
            : <button disabled={!this.state.validForm} type="submit" className="btn btn-primary">Submit</button>
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <form className="form-group" onSubmit={this.handleSubmit}>
                <label htmlFor="title">Title</label>
                <input className="form-control" id="title" name="title" placeholder="Title" onChange={this.handleChange} value={this.state.data.title}></input>
                <p className="danger">{this.state.error.title}</p>
                <label htmlFor="description">Description</label>
                <input className="form-control" id="description" name="description" placeholder="Description" onChange={this.handleChange} value={this.state.data.description}></input>
                <p className="danger">{this.state.error.description}</p>
                <label>Image</label>
                <img src={`${imgUrl}/${this.state.data.image}`} alt="feed_image.png" width="400px" ></img>
                <br></br>
                <label>Choose Image</label>
                <input type="file" className="form-control" name="new-image" onChange={this.handleChange} ></input>
                <br />
                {btn}
            </form>

        return (
            <>
                <h2>Update Blog</h2>
                {content}
            </>
        )
    }
}