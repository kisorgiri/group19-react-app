import React, { Component } from 'react';
import httpClient from '../../utils/http-client';
import notification from '../../utils/notification';
import { ViewFeedComponent } from './viewFeedCompoent';


export class SearchFeedComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                title: ''
            },
            error: {

            },
            isSubmitting: false,
            isValidForm: false,
            searchResult: [],
            allFeeds: [],
            isLoadig: true
        };
    }


    componentDidMount() {
        httpClient.post('/feed/search', {})
            .then((data) => {
                this.setState({
                    allFeeds: data.data
                })
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoadig: false
                })
            })
    }

    handleChange = (e) => {
        let { type, name, value } = e.target;
        if (type === 'checkbox') {
            value = e.target.checked
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {

        })
    }

    handleSubmit = (e) => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = null;
        }
        if (!data.toDate) {
            data.toDate = data.fromDate
        }
        httpClient.post('/feed/search', data)
            .then((data) => {
                console.log('data >>', data);
                if (data.data.length) {
                    this.setState({
                        searchResult: data.data,
                    });
                }
                else {
                    notification.showInfo("No any post matched your search query");
                }

            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                });
                notification.handleError(err);
            })
    }

    searchAgain = (e) => {
        this.setState({
            searchResult: [],
        })
    }


    render() {
        let titles = this.state.allFeeds.map(feed => (
            <option key={feed._id} value={feed.title}>{feed.title}</option>
        ));
        let toDateContent = this.state.data.multipleDateRange
            ?
            <>
                <br></br>
                <label>To Date</label>
                <input type="date" name="toDate" className="form-control" onChange={this.handleChange}></input>
            </>
            : '';
        let content = this.state.searchResult.length
            ? <>
                <button className="btn btn-primary" onClick={this.searchAgain}>Search Again</button>
                <ViewFeedComponent incomingData={this.state.searchResult}></ViewFeedComponent>
            </>
            : <form className="form-group" onSubmit={this.handleSubmit}>
                <label htmlFor="title">Title</label>
                <select className="form-control" id="title" onChange={this.handleChange} name="title">
                    <option value="">(Select Name)</option>
                    {titles}
                </select>
                <label>From Date</label>
                <input type="date" name="fromDate" className="form-control" onChange={this.handleChange}></input>
                <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                <label>Multiple Date Range</label>
                {toDateContent}
                <br></br>
                <button className="btn btn-primary" type="submit">Search</button>
            </form>

        return (
            <>
                <h2>Search Blogs</h2>
                {content}
            </>
        )
    }
}