import React, { Component } from 'react';
import './live-feed.component.css';
import { Loader } from '../../../utils/loader';
import { connect } from 'react-redux';
import FetchFeeds, { changePage } from './../../../actions/feeds/feed_actions';

class LiveFeed extends Component {

    constructor() {
        super();
        this.user = JSON.parse(localStorage.getItem('user'));

    }

    componentDidMount() {
        this.props.fetch();
        // this.httpCall();
    }

    componentWillReceiveProps() {
        console.log('this props when it recerives fresh props from state', this.props);
    }

    previousFeed = (e) => {
        console.log('check previous pagenumber >>', this.props.pageNumber);
        let currentPage = this.props.pageNumber - 1;
        this.props.fetch(currentPage);
        this.props.changePage(currentPage)

    }

    nextFeed = (e) => {
        let currentPage = this.props.pageNumber + 1;
        this.props.fetch(currentPage);;
        this.props.changePage(currentPage);
    }

    render() {
        let image_placeholder = `images/download.jpeg`

        const IMG_URL = process.env.REACT_APP_IMG_URL;
        let feedContent = this.props.data.map(feed => (
            <div key={feed._id} className="feed">
                <div className="feed-container">
                    <div className="feed-data">
                        <div className="feed-title">
                            <h1> {feed.title}</h1>
                        </div>
                        <p className="feed-description"> {feed.description}</p>
                    </div>

                    <div className="feed-image">
                        <img src={feed.image ? `${IMG_URL}/${feed.image}` : image_placeholder} alt="feedImage" width="200px"></img>
                    </div>
                </div>
                <div className="text-right">
                    <p><strong>{feed.user.username}</strong>  <span> <em>{feed.createdAt}</em></span></p>
                </div>
            </div>
        ))
        let mainContent = this.props.isLoading
            ? <Loader></Loader>
            : <>
                {feedContent}
                <div className="row">
                    <div className="col-md-6">
                        <button className="btn btn-success" onClick={this.previousFeed}>Previous</button>
                    </div>
                    <div className="col-md-6">
                        <button className="btn btn-success" onClick={this.nextFeed}>Next</button>
                    </div>
                </div>
            </>
        return (
            mainContent
        )
    }

}
// what components consume
const mapStateToProps = (state) => ({
    // fetch: FetchFeeds
    data: state.feeds.feeds,
    isLoading: state.feeds.isLoading,
    pageNumber: state.feeds.pageNumber
})
// what component can dispatch
const mapDispatchToProps = (dispatch) => ({
    fetch: (pageNumber, pageCount) => dispatch(FetchFeeds(pageNumber, pageCount)),
    changePage: (pageNumber) => dispatch(changePage(pageNumber)),

})
export const LiveFeedComponent = connect(mapStateToProps, mapDispatchToProps)(LiveFeed);