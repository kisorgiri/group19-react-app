import React, { Component } from 'react';
import httpClient from '../../utils/http-client';
import notification from '../../utils/notification';

export class ResetPassword extends Component {

    constructor() {
        super();
        this.state = {
            data: {

            },
            error: {

            },
            isValidForm: false,
            isSubmitting: false
        }
    }

    componentDidMount() {
        this.token = this.props.match.params.token;
        console.log('this token', this.token);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.post(`/auth/reset-password/${this.token}`, this.state.data)
            .then(data => {
                notification.showInfo("Password reset link sent to your email please check your inbox");
                this.props.history.push('/login');
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateErrors(name);
        })
    }
    validateErrors(fieldName) {
        let err = this.state.data[fieldName].length
            ? this.state.data[fieldName].length > 6
                ? ''
                : 'weak password'
            : 'requried field'
        this.setState((pre) => ({
            error: {
                [fieldName]: err
            }
        }), () => {
            this.validateForm(fieldName);
        })
    }
    validateForm(fieldName) {
        console.log('this.state ', this.state);
        if (this.state.data.password !== this.state.data.confirmPassword) {
            return this.setState({
                error: {
                    [fieldName]: 'Password didnot match'
                }
            })
        }

        let errors = Object
            .values(this.state.error)
            .filter(err => err);

        console.log('error s...', errors);
        let errorExists = errors.length > 0;

        this.setState({
            isValidForm: !errorExists
        });
    }


    render() {
        let btn = !this.state.isSubmitting
            ? <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit" >Submit</button>
            : <button disabled className="btn btn-info">submitting...</button>;
        return (
            <>
                <h2>Reset Password</h2>
                <p>Please Choose your password to reset </p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" onChange={this.handleChange}></input>
                    <p>{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" name="confirmPassword" className="form-control" onChange={this.handleChange}></input>
                    <p>{this.state.error.confirmPassword}</p>
                    <br></br>
                    {btn}
                </form>
            </>
        )
    }
}
