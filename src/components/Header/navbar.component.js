import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import './navbar.component.css';


const NavBar = (props) => {

    const logout = () => {
        localStorage.clear();
        props.history.push('/');
    }

    let content = props.isLoggedInUser
        ? <ul className="nav-list">
            <li className="nav-item">
                <NavLink to='/dashboard'>Home</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to='/profile'>Profile</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to='/setting'>Settings</NavLink>
            </li>
            <li className="nav-item">
                <button className="btn btn-success" onClick={logout}>logout</button>
            </li>
        </ul>
        : <ul className="nav-list">
            <li className="nav-item">
                <NavLink to='/login'>Login</NavLink>
            </li>
            <li className="nav-item">
                <NavLink to='/register'>Register</NavLink>
            </li>

        </ul>
    return (
        content
    )


}

export default withRouter(NavBar);