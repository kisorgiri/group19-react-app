import React from 'react';
import { Link } from 'react-router-dom';
import './sidebar.component.css';


export const Sidebar = (props) => {

    return (
        <>
            <ul className="sidebar-menu">
                <li className="sidebar-item">
                    <Link to="/add-blog">Add Blog</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/view-blog">View Blogs</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/profile">Profile</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/search-blog">Search Blog</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/chat">Messages</Link>
                </li>
            </ul>
        </>
    )
}

