import React, { Component } from 'react';
import * as io from 'socket.io-client';
import './chat.component.css';
import http from './../../utils/http-client';
import notification from './../../utils/notification';
import { relativeTime, formatTime } from './../../utils/common/utlil';

export class ChatComponent extends Component {
    constructor() {
        super();
        this.state = {
            senderId: null,
            receiverId: null,
            senderName: null,
            receiverName: null,
            message: '',
            time: null,
            messages: [],
            users: [],
            isUserLoading: true,
            isTyping: false,
            data: {},
        };
        this.user = JSON.parse(localStorage.getItem('user'));
    }

    componentDidMount() {
        // http.get('/api/user', true)
        //     .then((data) => {
        //         this.setState({
        //             users: data.data
        //         });
        //     })
        //     .catch(err => {
        //         notification.handleError(err);
        //     })
        //     .finally(() => {
        //         this.setState({
        //             isUserLoading: false
        //         })
        //     })
        this.socket = io(process.env.REACT_APP_SOCKET_URL);
        this.socket.emit('new-user', this.user.username);
        this.runSocket();
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        if (!this.state.receiverId) {
            return notification.showInfo('Please Select User to continue');
        }

        const data = {
            senderName: this.user.username,
            message: this.state.message,
            receiverId: this.state.receiverId,
            time: Date.now(),
        }
        this.state.users.forEach(user => {
            if (user.name === this.user.username) {
                data.senderId = user.id;
            }
        })
        this.socket.emit('new-msg', data);
        this.setState({
            message: '',
            data
        })
    }

    selectUser = (userId) => {
        this.setState({
            receiverId: userId
        });
    }

    focusedIn = (val) => {
        if (val) {
            this.socket.emit('is-typing', this.state.data)
        }
        else {
            this.socket.emit('is-typing-stop', this.state.data)

        }

    }


    runSocket() {
        this.socket.on('reply-msg-user', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages,
                receiverId: data.senderId
            });
        })
        this.socket.on('reply-msg-own', (data) => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages,
            });
        })
        this.socket.on('users', (data) => {
            this.setState({
                users: data
            });
        })
        this.socket.on('typing', (data) => {
            this.setState({
                isTyping: true,
            })
        })
        this.socket.on('typing-stop', (data) => {
            this.setState({
                isTyping: false,
            })
        })
    }

    render() {

        let users = this.state.users.map(user => (
            <li key={user.id}>
                <button onClick={() => this.selectUser(user.id)} className="btn btn-default">{user.name}</button>
            </li>
        ))
        let messages = this.state.messages.map((msg, i) => (
            <div key={i}>
                <h3>{msg.senderName}</h3>
                <p>{msg.message}</p>
                <small>{formatTime(msg.time)}</small>
            </div>
        ));
        let message = this.state.isTyping
            ? 'user is typing'
            : '';
        return (
            <>
                <h2>Let's Chat</h2>
                <div className="row">
                    <div className="col-md-6">
                        <p><strong>{this.user.username}</strong></p>
                        <div className="messages">
                            <ins>Messages</ins>
                            {messages}

                        </div>
                        <form className="form-group" onSubmit={this.handleSubmit}>
                            <input
                                name="message"
                                className="form_control"
                                type="text"
                                value={this.state.message}
                                onFocus={() => this.focusedIn(true)}
                                onBlur={() => this.focusedIn(false)}
                                placeholder="message here..."
                                onChange={this.handleChange}
                            ></input>
                            <button className="btn btn-primary" type="submit" >send</button>
                        </form>
                        <p>{message}</p>
                    </div>

                    <div className="col-md-2"></div>
                    <div className="col-md-4 messages">
                        <ins>Users</ins>
                        <ul>
                            {users}
                        </ul>
                    </div>
                </div>
            </>
        )
    }
}