import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import http from './../../utils/http-client';
import notification from './../../utils/notification';

import './register.component.css';
const defaultForm = {
    name: '',
    email: '',
    phone: '',
    address: '',
    username: '',
    password: '',
    gender: '',
    dob: '',
}
export class Register extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            validForm: false,
            isSubmitting: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true,
        })
        // http call (client server communciation)
        // correct use of http verb
        // data format must be in json
        http.post('/auth/register', this.state.data)
            .then((data) => {
                notification.showInfo("registration successful please login to continue")
                this.setState({
                    isSubmitting: false,
                });
                this.props.history.push('/login');
            })
            .catch(err => {
                notification.handleError(err);
                this.setState({
                    isSubmitting: false,
                });
            });
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState((previousState) => ({
            data: {
                ...previousState.data,
                [name]: value
            }
        }), () => {
            this.validateErrors(name)
        });
    }

    validateErrors(fieldName) {
        let errorMsg;
        switch (fieldName) {
            case 'name':
                errorMsg = this.state.data[fieldName]
                    ? ''
                    : 'Name is required';
                break;
            case 'email':
                errorMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@')
                        ? ''
                        : 'Not an valid Email'
                    : 'Email is required';
                break;
            default:
                errorMsg = '';

        }
        this.setState((errState) => ({
            error: {
                ...errState.error,
                [fieldName]: errorMsg
            }
        }), () => {
            this.validateForm();
        })
    }
    validateForm() {
        let btnEnabledStatus;
        const errors = Object
            .values(this.state.error)
            .filter(err => err);

        // errors.filter((item,i)=>{
        //     if(item){
        //         return item;
        //     }
        // })
        if (errors.length) {
            btnEnabledStatus = false;
        } else {
            btnEnabledStatus = true;
        }

        this.setState({
            validForm: btnEnabledStatus
        })

    }

    render() {

        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info" type="submit">submitting...</button>

            : <button disabled={!this.state.validForm} className="btn btn-primary" type="submit">Submit</button>

        return (
            <>
                <h2>Register</h2>
                <p>Welcome to Broadway Infosys Nepal</p>
                <p>Please provide necessary details for registration</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label htmlFor="name">name</label>
                    <input id="name" className="form-control" type="text" name="name" placeholder="name" onChange={this.handleChange}></input>
                    <p>{this.state.error.name}</p>
                    <label htmlFor="address">address</label>
                    <input id="address" className="form-control" type="text" name="address" placeholder="address" onChange={this.handleChange}></input>
                    <label htmlFor="email">email</label>
                    <input id="email" className="form-control" type="text" name="email" placeholder="email" onChange={this.handleChange}></input>
                    <p>{this.state.error.email}</p>
                    <label htmlFor="phone">phone</label>
                    <input id="phone" className="form-control" type="text" name="phone" placeholder="phone" onChange={this.handleChange}></input>
                    <label htmlFor="username">username</label>
                    <input id="username" className="form-control" type="text" name="username" placeholder="username" onChange={this.handleChange}></input>
                    <label htmlFor="password">password</label>
                    <input id="password" className="form-control" type="text" name="password" placeholder="password" onChange={this.handleChange}></input>
                    <label htmlFor="gender">gender</label>
                    <div>
                        <input className="gender" id="gender" type="radio" name="gender" value="male" onChange={this.handleChange} />Male
                        <input className="gender" id="gender1" type="radio" name="gender" value="female" onChange={this.handleChange} />Female
                        <input className="gender" id="gender2" type="radio" name="gender" value="others" onChange={this.handleChange} />Others
                    </div>
                    <br />
                    <label htmlFor="dob">dob</label>
                    <input id="dob" className="form-control" type="text" name="dob" placeholder="dob" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
                <p>Already Registered?</p>
                <p>back to <Link to="/">login</Link></p>
            </>
        )
    }
}