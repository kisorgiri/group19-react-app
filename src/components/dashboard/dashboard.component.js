import React, { Component } from 'react';
import './dashboard.component.css';
import { LiveFeedComponent } from '../feeds/live-feeds/live-feed.component';

export class DashboardComponent extends Component {

    constructor() {
        super();
        this.state = {};
        this.user = JSON.parse(localStorage.getItem('user'));
    }

    componentDidMount() {
        // this.user = JSON.parse(localStorage.getItem('user'));

    }

    render() {
        return (
            <>
                <p>Welcome to My App please use side navigation menu or contaact system administrator</p>
                <LiveFeedComponent />
            </>
        )
    }

}