import React, { Component } from 'react';
import httpClient from '../../utils/http-client';
import notification from '../../utils/notification';

export class ForgotPassword extends Component {

    constructor() {
        super();
        this.state = {
            data: {

            },
            error: {

            },
            isValidForm: false,
            isSubmitting: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.post('/auth/forgot-password', this.state.data)
            .then(data => {
                notification.showInfo("Password reset link sent to your email please check your inbox");
                this.props.history.push('/login');
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                [name]: value
            }
        }), () => {
            this.validateErrors(name);
        })
    }
    validateErrors(fieldName) {
        let err = this.state.data[fieldName].length
            ? this.state.data[fieldName].includes('@')
                ? ''
                : 'invalid email'
            : 'requried field'
        this.setState((pre) => ({
            error: {
                [fieldName]: err
            }
        }), () => {
            this.validateForm();
        })
    }
    validateForm() {
        let errors = Object
            .values(this.state.error)
            .filter(err => err);

        let errorExists = errors.length > 0;
        this.setState({
            isValidForm: errorExists
        });
    }


    render() {
        console.log('check ', this.state.isSubmitting);
        let btn = !this.state.isSubmitting
            ? <button disabled={this.state.isValidForm} className="btn btn-primary" type="submit" >Submit</button>
            : <button disabled className="btn btn-info">submitting...</button>;
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please enter your registered email to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Email</label>
                    <input type="text" name="email" className="form-control" onChange={this.handleChange}></input>
                    <p>{this.state.error.email}</p>
                    <br></br>
                    {btn}
                </form>
            </>
        )
    }
}
